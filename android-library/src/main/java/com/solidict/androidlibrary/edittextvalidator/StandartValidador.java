package com.solidict.androidlibrary.edittextvalidator;

import android.widget.EditText;

/**
 * Created by umutkina on 08/02/16.
 */
public class StandartValidador implements Validator {
    @Override
    public boolean validate(EditText editText) {

        return  editText.getText().toString().length()>0;
    }

    @Override
    public String getMessage(EditText editText) {

        return "Lütfen "+editText.getHint().toString()+" alanını boş bırakmayınız";
    }
}
