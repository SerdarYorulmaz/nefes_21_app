package com.solidict.androidlibrary.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Build;
import android.telephony.SmsManager;
import android.util.DisplayMetrics;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;


public class UtilsLibrary {


    // public static final String BASE_IMAGE_URL =
    // "http://cardgusto.solidict.com/";

    public static double roundTwoDecimals(double d) {
        DecimalFormat twoDForm = new DecimalFormat("#.##");
        return Double.valueOf(twoDForm.format(d));
    }

    public static String separeteDigit(int number) {

        String numberStr = number + "";
        int length = numberStr.length();
        StringBuilder stringBuilder = new StringBuilder();
        int modValue = length % 3;

        String str = numberStr.substring(modValue);
        for (int i = str.length(); i >= 3; i--) {

            if (i % 3 == 0) {
                System.out.println("counter : " + i);
                stringBuilder.insert(0, "." + str.substring(i - 3, i));
            }

        }
        String s1 = stringBuilder.toString();
        if (modValue == 0) {
            System.out.println("seperated value : " + s1);
            System.out.println("seperated value : 2 " + s1.substring(1));
            return s1.substring(1);
        } else {
            stringBuilder.insert(0, numberStr.substring(0, modValue) + ".", 0, modValue);
            return stringBuilder.toString();
        }


    }


    public static int getAge(String birthDate) {
        if (birthDate != null && birthDate.length() != 0) {
            String[] split = birthDate.split("\\.");
            String year = split[2];
            Calendar now = Calendar.getInstance(); // This gets the current date and
            // time.
            int currentYear = now.get(Calendar.YEAR);
            int birthYear = Integer.parseInt(year);
            return currentYear - birthYear;
        } else {
            return 0;
        }
    }


    public static String convertToParameter(HashMap<String, String> parameters) {
        StringBuilder parameter = new StringBuilder();

        for (Map.Entry<String, String> entry : parameters.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            parameter.append("&" + key + "=" + value);
        }
        if (parameters.size() > 0) {
            return parameter.toString().substring(1,
                    parameter.toString().length());
        } else {
            return null;
        }

    }


    public static boolean isAppropriateEmail(String email) {
        if (email.lastIndexOf(".") > email.indexOf("@")
                && email.indexOf("@") > 0) {
            return true;
        }
        return false;
    }

    public static void showInfoDialogWithEdittext(Context context, final Runnable onOk,
                                                  String title, String message, String hint) {

        final AlertDialog.Builder alert = new AlertDialog.Builder(context);

        alert.setTitle(title);
        alert.setMessage(message);
        final EditText input = new EditText(context);
        input.setHint(hint);
        alert.setView(input);
        if (onOk != null) {
            alert.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub

                        }
                    });
        }
        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                if (onOk != null) {
                    onOk.run();
                }
            }
        });

        try {
            ((Activity) context).runOnUiThread(new Runnable() {
                public void run() {
                    alert.show();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showInfoDialog(Context context, final Runnable onOk,
                                      String title, String message) {

        final AlertDialog.Builder alert = new AlertDialog.Builder(context);

        alert.setTitle(title);
        alert.setMessage(message);

        if (onOk != null) {
            alert.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub

                        }
                    });
        }
        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                if (onOk != null) {
                    onOk.run();
                }
            }
        });

        try {
            ((Activity) context).runOnUiThread(new Runnable() {
                public void run() {
                    alert.show();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showInfoDialogWithRunnable(Context context, final Runnable onOk,
                                                  String title, String message) {

        final AlertDialog.Builder alert = new AlertDialog.Builder(context);

        alert.setTitle(title);
        alert.setMessage(message);
        alert.setCancelable(false);
        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                if (onOk != null) {
                    onOk.run();
                }
            }
        });

        try {
            ((Activity) context).runOnUiThread(new Runnable() {
                public void run() {
                    alert.show();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void browse(String url, Context context) {

        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        context.startActivity(i);

    }

    public static void Call(final String tell, final Context context) {
        Runnable runnable = new Runnable() {

            @Override
            public void run() {
                String number = "tel:" + tell;
                Intent callIntent = new Intent(Intent.ACTION_CALL,
                        Uri.parse(number));
                try {
                    context.startActivity(callIntent);
                } catch (ActivityNotFoundException ex) {
                    Toast.makeText(context,
                            "there are no call clients installed",
                            Toast.LENGTH_SHORT).show();
                }

            }

        };
        showInfoDialog(context, runnable, "Bilgi",
                "Aramak istediğinize emin misiniz?");
    }

    public static void mail(String mail, Activity context) {
        try {
            Intent mailIntent = new Intent(Intent.ACTION_SENDTO);


            String uriText = "mailto:" + Uri.encode("contact@petsleepover.com.au") + "?subject="
                    + Uri.encode("PetSleepover") + "&body=" + Uri.encode(mail);
            Uri uri = Uri.parse(uriText);

            mailIntent.setData(uri);

            context.startActivity(mailIntent);

        } catch (final ActivityNotFoundException e) {
            Toast.makeText(context,
                    "You don't seem to have twitter installed on this device",
                    Toast.LENGTH_SHORT).show();
        }

    }

    public static LinkedHashMap<String, String> readFile(
            Context context) {
        LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
        AssetManager am = null;
        am = context.getAssets();

        InputStream inputStream = null;
        try {
            inputStream = am.open("city_new.txt");

            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    inputStream, "UTF-8"));

            // StringBuilder out = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                // out.append(UTF8Str); // add everything to StringBuilder
                // here you can have your logic of comparison.
                String[] lines = line.split("\t");
                int parseInt = Integer.parseInt(lines[0]);
                map.put(parseInt + "", lines[1]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return map;

    }


    public static int dpToPx(int dp, Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    public static void sendSMS(String phoneNumber, String message, final Context context) {
        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(context, 0, new Intent(
                SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(context, 0,
                new Intent(DELIVERED), 0);

        // ---when the SMS has been sent---
        context.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(context, "SMS sent",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(context, "Generic failure",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(context, "No service",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(context, "Null PDU",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(context, "Radio off",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(SENT));

        // ---when the SMS has been delivered---
        context.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(context, "SMS delivered",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(context, "SMS not delivered",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(DELIVERED));

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
    }


    public static void startNewMainActivity(Activity currentActivity, Class<? extends Activity> newTopActivityClass) {
        Intent intent = new Intent(currentActivity, newTopActivityClass);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            intent.addFlags(0x8000); // equal to Intent.FLAG_ACTIVITY_CLEAR_TASK which is only available from API level 11
        currentActivity.startActivity(intent);
    }


    public static void showKeyboard(EditText mEtSearch, Context context) {
        mEtSearch.requestFocus();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }

    public static void hideSoftKeyboard(EditText mEtSearch, Context context) {
        mEtSearch.clearFocus();
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mEtSearch.getWindowToken(), 0);


    }

    public static String getCurrentTimeStamp() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd.MM.yyyy");//dd/MM/yyyy
        Date now = new Date();
        String strDate = sdfDate.format(now);
        return strDate;
    }

    public static String getTomorrowDate() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd.MM.yyyy");//dd/MM/yyyy
        Date today = new Date();
        Date tomorrow = new Date(today.getTime() + (1000 * 60 * 60 * 24));
        String strDate = sdfDate.format(tomorrow);
        return strDate;
    }

    public static String getAfterTomorrowDate() {
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd.MM.yyyy");//dd/MM/yyyy
        Date today = new Date();
        Date tomorrow = new Date(today.getTime() + (1000 * 60 * 60 * 24 * 2));
        String strDate = sdfDate.format(tomorrow);
        return strDate;
    }
}
