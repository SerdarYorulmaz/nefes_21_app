package com.solidict.androidlibrary.views.valiadtorview;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.CheckBox;

import com.solidict.androidlibrary.R;
import com.solidict.androidlibrary.edittextvalidator.ValidateableView;
import com.solidict.androidlibrary.edittextvalidator.ValidatorUtils;

/**
 * Created by umutkina on 08/02/16.
 */
public class ValidateableCheckBox extends CheckBox implements ValidateableView{

    String message;
    boolean validate;

    public ValidateableCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray styledAttrs = context.obtainStyledAttributes(attrs, R.styleable.ValidateableCheckBox);
        if (styledAttrs.hasValue(R.styleable.ValidateableCheckBox_validatorTypeCheckbox)) {
            int anInt = styledAttrs.getInt(R.styleable.ValidateableCheckBox_validatorTypeCheckbox, 0);

            switch (anInt) {
                case 0:
                    validate = false;
                    break;
                case 1:
                    validate = true;
                    break;


            }
        }
    }


    @Override
    public boolean validate() {
        if (!validate) {
            return true;
        }
        if (!isChecked()) {
            if (message == null) {
                message = (String) "Lütfen "+ getText().toString()+"'ni onaylayınız";
            }


            ValidatorUtils.showWarningDialog(getContext(), message);

            return false;
        }
        return true;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isValidate() {
        return validate;
    }

    public void setValidate(boolean validate) {
        this.validate = validate;
    }
}
