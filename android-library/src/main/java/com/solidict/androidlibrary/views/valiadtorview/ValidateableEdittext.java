package com.solidict.androidlibrary.views.valiadtorview;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.EditText;

import com.solidict.androidlibrary.R;
import com.solidict.androidlibrary.edittextvalidator.EmailValidator;
import com.solidict.androidlibrary.edittextvalidator.StandartValidador;
import com.solidict.androidlibrary.edittextvalidator.ValidateableView;
import com.solidict.androidlibrary.edittextvalidator.Validator;
import com.solidict.androidlibrary.edittextvalidator.ValidatorUtils;

/**
 * Created by umutkina on 08/02/16.
 */
public class ValidateableEdittext extends EditText implements ValidateableView {
    Validator validator = new StandartValidador();

    public ValidateableEdittext(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray styledAttrs = context.obtainStyledAttributes(attrs, R.styleable.ValidateableEdittext);
        if (styledAttrs.hasValue(R.styleable.ValidateableEdittext_validatorType)) {
            int anInt = styledAttrs.getInt(R.styleable.ValidateableEdittext_validatorType, 0);

            switch (anInt) {
                case 0:
                    validator=null;
                    break;
                case 1:
                    validator=new StandartValidador();
                    break;
                case 2:
                    validator=new EmailValidator();
                    break;

            }
        }

    }


    public boolean validate() {
        if (validator == null) {
            return  true;
        }
        boolean validate = this.validator.validate(this);
        if (!validate) {
            ValidatorUtils.showWarningDialog(getContext(), validator.getMessage(this));
        }
        return validate;
    }


    public Validator getValidator() {
        return validator;
    }

    public void setValidator(Validator validator) {
        this.validator = validator;
    }
}
