package com.solidict.androidlibrary.views.valiadtorview;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.Spinner;

import com.solidict.androidlibrary.R;
import com.solidict.androidlibrary.edittextvalidator.ValidateableView;
import com.solidict.androidlibrary.edittextvalidator.ValidatorUtils;

/**
 * Created by umutkina on 08/02/16.
 */
public class ValidateableSpinner extends Spinner implements ValidateableView {

    public boolean validate = true;
    String message;

    public ValidateableSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray styledAttrs = context.obtainStyledAttributes(attrs, R.styleable.ValidateableSpinner);
        if (styledAttrs.hasValue(R.styleable.ValidateableSpinner_validatorTypeSpinner)) {
            int anInt = styledAttrs.getInt(R.styleable.ValidateableSpinner_validatorTypeSpinner, 0);

            switch (anInt) {
                case 0:
                    validate = false;
                    break;
                case 1:
                    validate = true;
                    break;


            }
        }
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public boolean validate() {
        if (!validate) {
            return true;
        }
        if (getSelectedItemPosition() == 0) {

            if (message == null) {
                message = (String) getSelectedItem();
            }


            ValidatorUtils.showWarningDialog(getContext(), message);

            return false;
        }
        return true;
    }
}
