package com.example.nefes_21_app.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.nefes_21_app.R
import com.example.nefes_21_app.adapter.ContentViewPagerAdapter
import com.example.nefes_21_app.ui.text.TextFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_show.*

class ShowActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show)

        setupUI()
    }

    private  fun setupUI(){
        setSupportActionBar(showToolbar)
        setupViewPager()
        showTabs.setupWithViewPager(main_viewpager)
    }

    private fun setupViewPager(){
        val adapter= ContentViewPagerAdapter(supportFragmentManager)
        adapter.apply {
            addFragment(TextFragment(),"TextContent")
           // addFragment(TopRatedMoviesFragment(),"Top Rated")
        }

        showViewpager.adapter=adapter
    }
}
