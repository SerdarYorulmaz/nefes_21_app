package com.example.nefes_21_app.ui

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager

import com.example.nefes_21_app.R
import com.example.nefes_21_app.adapter.HomePageAdapter
import com.example.nefes_21_app.model.HomeMenu
import kotlinx.android.synthetic.main.fragment_home_menu.*

class HomeMenuFragment : Fragment() {

    private var menu=ArrayList<HomeMenu>()
    private lateinit var myAdapter: HomePageAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home_menu, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        dataSource()



        menu_recyclerview.layoutManager=GridLayoutManager(activity,2)
        menu_recyclerview.adapter=HomePageAdapter(menu){
           // Toast.makeText(activity,"${it.name}",Toast.LENGTH_SHORT).show()
            if(it.name=="Yazılar" || it.name=="Videolar"){
                val intent=Intent(activity,ShowActivity::class.java)
                startActivity(intent)
            }else Toast.makeText(activity,"Diger Menuler Aktif Degil!!",Toast.LENGTH_LONG).show()

        }

//        var layoutManager= GridLayoutManager(activity,2)
//        menu_recyclerview.layoutManager=layoutManager
    }

    fun openActivityListItems(position : Int)
    {

//        menu_recyclerview=item
//        var intent=Intent(context,ShowActivity::class.java)
//        context?.startActivity(intent)


    }

    private fun dataSource(){

        var homePageIcons=arrayOf(
            R.drawable.home_writing,R.drawable.home_video,
            R.drawable.home_about,R.drawable.home_activity,
            R.drawable.home_seminar,R.drawable.home_book,
            R.drawable.home_consultancy,R.drawable.home_card,
            R.drawable.home_workshop,R.drawable.home_contact
        )

        var homePageTexts= arrayOf(
            "Yazılar", "Videolar", "Hakkımızda", "Etkinlikler", "Bireysel Terapi",
            "Kitap/CD", "\nYolculuğa Başla\n", "Olumlama\nFarkındalık\nKartları",
            "Atölyeler", "İletişim"
        )

        for (i in 0..homePageIcons.size-1){
            var addMenu= HomeMenu(homePageTexts[i],homePageIcons[i])
            menu.add(addMenu)
        }

    }

}
