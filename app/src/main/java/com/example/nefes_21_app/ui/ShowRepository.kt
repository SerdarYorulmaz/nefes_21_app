package com.example.nefes_21_app.ui

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.nefes_21_app.data.ApiClient
import com.example.nefes_21_app.data.ApiService
import com.example.nefes_21_app.model.remote.TextContent
import com.example.nefes_21_app.model.remote.TextContentResponse
import com.example.nefes_21_app.model.remote.VideoContent
import com.example.nefes_21_app.model.remote.VideoContentResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ShowRepository {


    /**  lazy ile tek satirda yapiyorz normalde init tanimlayip esitlemek geerkecekti*/

    private val apiService: ApiService by lazy { ApiClient.getApiService() }


    fun getHomeText(): LiveData<List<TextContent>>? {

        val textLiveData: MutableLiveData<List<TextContent>> = MutableLiveData()

        apiService.getHomeText().enqueue(object : Callback<TextContentResponse> {
            override fun onFailure(call: Call<TextContentResponse>, t: Throwable) {

                /** hatayi log olarak gosteriyor */
                Log.e("getHomeText", t.message)
            }

            override fun onResponse(call: Call<TextContentResponse>, response: Response<TextContentResponse>) {

                /** Burdada serviste gelen response model aktırdıgından modeldeki veriyi livedata vaolu oarak aktsriyoruz */

                textLiveData.value = response.body()?.contents
            }

        })

        return textLiveData

    }

    fun getHomeVideo(): LiveData<List<VideoContent>>? {

        val videoLiveData: MutableLiveData<List<VideoContent>> = MutableLiveData()

        apiService.getHomeVideo().enqueue(object : Callback<VideoContentResponse> {
            override fun onFailure(call: Call<VideoContentResponse>, t: Throwable) {

                Log.e("getHomeVideo",t.message)
            }

            override fun onResponse(call: Call<VideoContentResponse>, response: Response<VideoContentResponse>) {

                videoLiveData.value=response.body()?.videoContents
            }

        })
        return  videoLiveData

    }
}