package com.example.nefes_21_app.ui.text

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.nefes_21_app.model.remote.TextContent
import com.example.nefes_21_app.ui.ShowRepository

class TextViewModel:ViewModel() {

    private  val repository:ShowRepository by lazy {  ShowRepository() }

    fun getHomeText():LiveData<List<TextContent>>?=repository.getHomeText()
}