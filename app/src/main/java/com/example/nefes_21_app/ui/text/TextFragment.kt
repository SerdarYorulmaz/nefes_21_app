package com.example.nefes_21_app.ui.text

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager

import com.example.nefes_21_app.R
import com.example.nefes_21_app.adapter.TextAdapter
import com.example.nefes_21_app.common.BaseVMFragment
import com.example.nefes_21_app.util.gone
import com.example.nefes_21_app.util.visible
import kotlinx.android.synthetic.main.fragment_text.*


class TextFragment : BaseVMFragment<TextViewModel>() {

    private lateinit var  adapter:TextAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_text, container, false)
    }

    override fun getViewModel(): Class<TextViewModel> = TextViewModel::class.java

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        adapter= TextAdapter()
        textRecyclerview.layoutManager= activity?.let { GridLayoutManager(it,2) }

        viewModel.getHomeText()?.observe(viewLifecycleOwner, Observer {
            adapter.submitList(it)
            textRecyclerview.adapter=adapter
            textRecyclerview.visible()
            textProgressbar.gone()

        })
    }


}
