package com.example.nefes_21_app.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.nefes_21_app.R
import com.example.nefes_21_app.adapter.HomePageAdapter
import com.example.nefes_21_app.adapter.HomeViewPagerAdapter
import com.example.nefes_21_app.model.HomeMenu
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)




        setupUI()

    }


    private fun setupUI() {
        setSupportActionBar(main_toolbar)
        setupViewPager()

        main_toolbar.setLogo(R.mipmap.b_ga)

    }

    private fun setupViewPager() {
        val adapter = HomeViewPagerAdapter(supportFragmentManager)
        adapter.apply {
            addFragment(HomeMenuFragment(), "Menu")

        }

        main_viewpager.adapter = adapter

    }

}
