package com.example.nefes_21_app.common

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders

abstract class BaseActivity<DB:ViewDataBinding,VM:ViewModel>:AppCompatActivity() {

    lateinit var dataBinding: DB
    lateinit var viewModel: VM

    @LayoutRes
    abstract fun getLayoutRes():Int   /** bu method bize R.layout.activity_main gibi islemlerde ise yarayacak @Layout dosyasi oldugunu bidirmek icin LayoutRes kullailir.*/

    abstract fun getViewModel():Class<VM>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        /** databind tanimliyoruz */

        dataBinding= DataBindingUtil.setContentView(this,getLayoutRes()) /** alt class larda getlayoutres=R.layout.activiy gibi yaptigimzda direk base class gelecek */
        viewModel= ViewModelProviders.of(this).get(getViewModel())
    }
}