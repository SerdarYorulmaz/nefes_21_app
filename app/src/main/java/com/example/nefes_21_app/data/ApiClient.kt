package com.example.nefes_21_app.data

import com.example.nefes_21_app.util.Constants
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiClient {

    fun getApiService(): ApiService {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())


            .build()

        return retrofit.create(ApiService::class.java)
    }
}