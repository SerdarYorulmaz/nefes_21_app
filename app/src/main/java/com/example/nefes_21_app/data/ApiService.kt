package com.example.nefes_21_app.data

import com.example.nefes_21_app.model.remote.*
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {

    @GET("content/home-t/mobile-new")
    fun getHomeText(): Call<TextContentResponse>

    @GET("content/home-v/mobile-new")
    fun getHomeVideo(): Call<VideoContentResponse>


//    @GET("content/{contentId}/related-contents")
//    fun getRelatedContent(@Path("contentId") contentId:Long):Call<List<PartContent>>


}