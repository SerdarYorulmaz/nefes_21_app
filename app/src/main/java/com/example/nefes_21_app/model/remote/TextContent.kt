package com.example.nefes_21_app.model.remote

import com.example.nefes_21_app.model.remote.PartContent
import com.google.gson.annotations.SerializedName

data class TextContent(

    @SerializedName("id")
    var id:Long,
    @SerializedName("type")
    var type:Int,

    @SerializedName("image")
    var image:String,

    @SerializedName("title")
    var title:String,

    @SerializedName("Content")
    var content:String,

    @SerializedName("priority")
    var priority:Long,

    @SerializedName("date")
    var date:String,

    @SerializedName("parts")
    var textParts:List<PartContent>




) {
}

