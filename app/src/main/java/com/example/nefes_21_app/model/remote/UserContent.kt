package com.example.nefes_21_app.model.remote

import com.google.gson.annotations.SerializedName

data class UserContent(

    @SerializedName("id")
    var id:Long,

    @SerializedName("nameSurname")
    var name:String,

    @SerializedName("image")
    var image:String

) {
}