package com.example.nefes_21_app.model.remote

import com.example.nefes_21_app.model.remote.PartContent
import com.google.gson.annotations.SerializedName

data class VideoContent(

    @SerializedName("id")
    var id:Long,
    @SerializedName("type")
    var type:Int,

    @SerializedName("image")
    var image:String,

    @SerializedName("videoId")
    var videoId:String,

    @SerializedName("videoUrlWeb")
    var videoUrlWeb:String,

    @SerializedName("videoUrlMobile")
    var videoUrlMobile:String,

    @SerializedName("title")
    var title:String,

    @SerializedName("Content")
    var content:String,

    @SerializedName("priority")
    var priority:Long,

    @SerializedName("date")
    var date:String,

    @SerializedName("parts")
    var videoParts:List<PartContent>

) {
}