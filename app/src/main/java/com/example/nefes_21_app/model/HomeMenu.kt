package com.example.nefes_21_app.model

import android.os.Parcel
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class HomeMenu(
    var name: String?, var img: Int):Parcelable