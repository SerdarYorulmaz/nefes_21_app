package com.example.nefes_21_app.model.remote

import com.google.gson.annotations.SerializedName

data class VideoContentResponse(

    @SerializedName("contents")
    var videoContents:List<VideoContent>
) {
}