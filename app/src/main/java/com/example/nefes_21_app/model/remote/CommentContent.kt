package com.example.nefes_21_app.model.remote

import com.google.gson.annotations.SerializedName

data class CommentContent(

    @SerializedName("id")
    var id:Long,

    @SerializedName("type")
    var type:Int,

    @SerializedName("date")
    var date:String,

    @SerializedName("comment")
    var comment:String,

    @SerializedName("user")
    var user: UserContent

//TODO USER



) {
}