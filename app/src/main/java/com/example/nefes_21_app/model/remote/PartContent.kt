package com.example.nefes_21_app.model.remote

import com.google.gson.annotations.SerializedName

data class PartContent(

    @SerializedName("type")
    var type:Int,

    @SerializedName("text")
    var text:String

) {
}