package com.example.nefes_21_app.model.remote

import com.google.gson.annotations.SerializedName

data class TextContentResponse(
    @SerializedName("contents")
    var contents: List<TextContent>
) {
}