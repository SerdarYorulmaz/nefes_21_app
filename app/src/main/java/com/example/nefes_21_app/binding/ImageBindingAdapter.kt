package com.example.nefes_21_app.binding

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide

object ImageBindingAdapter {

    /** @JvmStatic yazmamisin sebebi classlar compiler edilirken bu methodun static olmasi icin JVM icin  */

    @BindingAdapter("imageUrl")
    @JvmStatic
    fun loadImage(imageView: ImageView, url:String){

        /**
         *
         * "https://nefes21.com/uploaded/5d4c1146-49e3-11e8-aaa5-d8cb8ac7459f.jpg",
         * degisen kisim:5d4c1146-49e3-11e8-aaa5-d8cb8ac7459f.jpg*/

        if(url.isNotEmpty()){
            Glide.with(imageView.context)
                .load(url)
                .into(imageView)
        }
    }
}