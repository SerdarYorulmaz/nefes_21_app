package com.example.nefes_21_app.util

import android.view.View

fun View.visible(){
    this.visibility= View.VISIBLE
}

fun View.gone(){
    this.visibility= View.GONE
}