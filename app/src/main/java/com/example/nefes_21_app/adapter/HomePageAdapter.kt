package com.example.nefes_21_app.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.nefes_21_app.R
import com.example.nefes_21_app.model.HomeMenu
import kotlinx.android.synthetic.main.item_homepage.view.*

class HomePageAdapter(menu:ArrayList<HomeMenu>,val listener: (HomeMenu) -> Unit):RecyclerView.Adapter<HomePageAdapter.ViewHolder>() {

//    companion object{
//        var mClickListener:ItemClickListener?=null
//    }
//
//    interface ItemClickListener
//    {
//        fun clickRow(position: Int)
//    }



   private var homeMenu=menu

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var inflater=LayoutInflater.from(parent.context)
        var menuItem=inflater.inflate(R.layout.item_homepage,parent,false)

        return ViewHolder(menuItem)
    }

    override fun getItemCount(): Int = homeMenu.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
       var tempData=homeMenu[position]
        holder.setData(tempData,position,listener)

//        holder?.itemView?.setOnClickListener {
//            mClickListener?.clickRow(position)
//        }
    }

     inner class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView){

       var menu=itemView as ConstraintLayout

       var menuTitle=menu.tv_homepage_item as TextView
       var menuImage=menu.iv_homepage_item as ImageView

       // itemView.setOnClickListener( {itemClick(layoutPosition)} )

       fun setData(item:HomeMenu, position: Int, listener: (HomeMenu) -> Unit)= with(itemView){
           menuTitle.setText(item.name)
           menuImage.setImageResource(item.img)
           setOnClickListener {
               (this@HomePageAdapter.listener)(item)
           }
       }
    }


}