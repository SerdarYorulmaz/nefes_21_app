package com.example.nefes_21_app.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.nefes_21_app.databinding.ItemTextBinding
import com.example.nefes_21_app.model.remote.TextContent

class TextAdapter : ListAdapter<TextContent, TextAdapter.ViewHolder>(DIFF_CALLBACK) {

        /** normalde view pasliyordukburda binding pasladik */
    class ViewHolder(val binding: ItemTextBinding) : RecyclerView.ViewHolder(binding.root) {
        companion object{
            fun create(inflater:LayoutInflater,parent: ViewGroup):ViewHolder{
                val itemTextBinding=ItemTextBinding.inflate(inflater,parent,false)
                return ViewHolder(itemTextBinding)
            }
        }

            fun bind(textContent: TextContent){
                /** bind ettik */
                binding.textContent=textContent
                binding.executePendingBindings()
            }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder.create(
        LayoutInflater.from(parent.context),parent)




    override fun onBindViewHolder(holder: ViewHolder, position: Int) =holder.bind(getItem(position))


    companion object {
        val DIFF_CALLBACK = object : DiffUtil.ItemCallback<TextContent>() {
            override fun areItemsTheSame(oldItem: TextContent, newItem: TextContent): Boolean {
                /** objeleri karsilastirir ona gore haber verir */
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: TextContent, newItem: TextContent): Boolean {
                /** icerikleri  karsilastirir ona gore haber verir   */

                return  oldItem.title == newItem.title
            }

        }

    }
}